import * as vscode from 'vscode';
import { API, GitExtension, Repository } from './external_api/git';
import Anthropic from '@anthropic-ai/sdk';
import { AnthropicAuthenticationProvider } from './anthropic_auth_provider';

const PROOFREAD_COMMAND = 'proofer.proofread';

interface Prompt {
  name: string;
  systemPrompt: string;
}

const proofread = async (
  systemPrompt: string,
  text: string,
  model?: string
): Promise<string> => {
  const session = await vscode.authentication.getSession(
    'anthropic',
    ['hello'],
    {
      createIfNone: true,
    }
  );
  if (!session) {
    throw new Error("Can't get anthropic token");
  }
  const anthropic = new Anthropic({ apiKey: session.accessToken });
  const msg = await anthropic.messages.create({
    model: model ?? 'claude-3-5-sonnet-20240620',
    max_tokens: 8192,
    temperature: 0,
    system: systemPrompt,
    messages: [
      {
        role: 'user',
        content: [
          {
            type: 'text',
            text,
          },
        ],
      },
    ],
  });
  console.log(msg);

  const response = msg.content[0].text;
  // return response?.split('%%%%%').pop()?.trim() || '';
  return response || '';
};

const replaceText = async (
  textDocument: vscode.TextDocument,
  newContent: string
) => {
  const edit = new vscode.WorkspaceEdit();
  edit.replace(
    textDocument.uri,
    new vscode.Range(
      textDocument.positionAt(0),
      textDocument.positionAt(textDocument.getText().length)
    ),
    newContent
  );
  await vscode.workspace.applyEdit(edit);
  await textDocument.save();
};

const getPrompt = async (): Promise<string | undefined> => {
  const prompts = vscode.workspace
    .getConfiguration('proofer')
    .get<Prompt[]>('prompts');

  if (!prompts || prompts.length < 1) {
    vscode.window.showErrorMessage(
      'The proofer.prompts in the settings.json is missing or empty'
    );
    return;
  }

  let prompt;

  if (prompts.length === 1) {
    prompt = prompts[0];
  } else {
    prompt = await vscode.window.showQuickPick(
      prompts.map((p) => ({ ...p, label: p.name }))
    );
  }
  return prompt?.systemPrompt;
};

const openGitChange = async (repo: Repository, uri: vscode.Uri) => {
  const onStateChange = repo.state.onDidChange(() => {
    vscode.commands.executeCommand('git.openChange', uri);
    onStateChange.dispose();
  });
};

const createProofreadCommand = (git: API) => async () => {
  const textDocument = vscode.window.activeTextEditor?.document;
  if (!textDocument) {
    return '';
  }
  const repo = git.getRepository(textDocument.uri);
  if (!repo) {
    vscode.window.showErrorMessage(
      'the current text file is not a part of a repository'
    );
    return;
  }
  await repo.status();
  // TODO possibly check that there are no changes instead of just this file ¯\_(ツ)_/¯
  const tdChange = repo.state.workingTreeChanges.find(
    (c) => c.uri.toString() === textDocument.uri.toString()
  );
  if (tdChange) {
    vscode.window.showErrorMessage(
      'the current text file has uncommitted changes. aborting'
    );
    return;
  }

  const prompt = await getPrompt();

  if (!prompt) {
    // get prompt already warns user if something's wrong
    return;
  }
  const newFileContent = await vscode.window.withProgress<string>(
    {
      location: vscode.ProgressLocation.Notification,
      title: 'Fetching suggestion from the model',
    },
    () => proofread(prompt, textDocument.getText())
  );

  await replaceText(textDocument, newFileContent);

  openGitChange(repo, textDocument.uri);
};

export function activate(context: vscode.ExtensionContext) {
  vscode.authentication.registerAuthenticationProvider(
    'anthropic',
    'Anthropic',
    new AnthropicAuthenticationProvider(context.secrets)
  );
  const gitExtension =
    vscode.extensions.getExtension<GitExtension>('vscode.git')?.exports;
  if (!gitExtension) {
    vscode.window.showErrorMessage(
      "Can't load Git extension. Make sure the SCM panel works in your window."
    );
    return;
  }
  const git = gitExtension.getAPI(1);

  let disposable = vscode.commands.registerCommand(
    PROOFREAD_COMMAND,
    createProofreadCommand(git)
  );

  context.subscriptions.push(disposable);
}

// This method is called when your extension is deactivated
export function deactivate() {}
