The main idea of the article is to discuss the implementation challenges and potential solutions for providing multiple suggestion options in the GitLab VS Code extension using the Language Server Protocol (LSP).

Subheaders and their contribution to the main idea:

1. "LSP intended use" - Explains how the LSP is designed to handle suggestions, highlighting the difference between automatic and invoked triggers.

2. "What does that mean for us" - Introduces two possible approaches to address the challenge: embracing or fighting the LSP.

3. "Embrace" - Describes the benefits and drawbacks of accepting the LSP's design and making an additional request when the user wants to cycle through suggestions.

4. "Fight" - Outlines the advantages and disadvantages of working around the LSP to provide multiple suggestion options instantly.

Suggestions for improvement:

1. Flow of ideas in subheaders:
   - The subheaders effectively break down the main idea into logical sections, making it easy to follow the thought process.
   - Consider adding a brief conclusion or recommendation subheader to summarize the preferred approach based on the analysis.

2. Flow of paragraphs:
   - The paragraphs within each subheader are well-structured and provide relevant information to support the main idea.
   - In the "Embrace" and "Fight" sections, consider adding a brief introductory paragraph to clearly state the approach being discussed before diving into the benefits and drawbacks.

%%%%%

## Status update 2024-05-17

**TL;DR: We should probably make an additional request when the user wants to cycle through suggestions**

Implementing the UI for multiple suggestion options in the GitLab VS Code extension is a bit more complex than initially thought.

- My original thought - We'll be sending [4 suggestion options](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1325#note_1908264625) every time we now send one
- Reality - That's not how the Language Server Protocol (LSP) is designed

### LSP intended use

- The Language Server is supposed to return a single option as the user types
- **Only when the user hovers over the inline suggestions (or when they press <kbd>Option</kbd>+<kbd>]</kbd>)** the VS Code asks the Language Server "Give me more suggestions", and the Language Server is supposed to answer with many (e.g., 10) suggestions

From the docs:

> - automatic trigger - Completion was triggered automatically while editing. It is sufficient to return a single completion item in this case.
> - invoked trigger - Completion was triggered explicitly by a user gesture. Return multiple completion items to enable cycling through them.

<details><summary>Docs</summary>

```ts
/**
 * Describes how an {@link InlineCompletionItemProvider inline completion
 * provider} was triggered.
 *
 * @since 3.18.0
 */
export namespace InlineCompletionTriggerKind {
	/**
	 * Completion was triggered explicitly by a user gesture.
	 * Return multiple completion items to enable cycling through them.
	 */
	export const Invoked: 1 = 1;

	/**
	 * Completion was triggered automatically while editing.
	 * It is sufficient to return a single completion item in this case.
	 */
	export const Automatic: 2 = 2;
}
```

[source](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.18/specification/#textDocument_inlineCompletion)

</details>

### What does that mean for us

We can either **embrace** or **fight** the LSP.

#### Embrace

Embracing the LSP means accepting that we'll make an additional request when the user expresses they want multiple suggestions.

- Benefits
  - Little to no extra cost because we'll only generate multiple suggestion options when the user wants them
  - We can generate more suggestions for the user (instead of the planned 4)
  - (Implementation-wise): Easy to track that users made the extra request
- Drawback
  - The user has to wait extra time before getting those options

Example user interaction

1. User typed text
2. Language Server fetches and returns a single suggestion ![Screenshot_2024-05-17_at_15.28.51](/uploads/0b8640bb70e2c485cb01ce9af4118d0c/Screenshot_2024-05-17_at_15.28.51.png)
3. The user indicates they want to cycle through (either hover over the suggestion or pressing <kbd>Option</kbd>+<kbd>]</kbd>)
4. Language Server fetches and returns a suggestion with many 10+ options (this step takes extra time because of an API call) ![Screenshot_2024-05-17_at_15.29.03](/uploads/1c3b8eb763614bcbf13be01f8eb65322/Screenshot_2024-05-17_at_15.29.03.png)

#### Fight

Fighting the LSP involves trying to work around it to provide multiple suggestion options instantly. This would involve some combination of caching and tracing the user's cursor.

- Benefits
  - The moment user wants to cycle through the options, they'll be instantly ready
- Drawbacks
  - From streaming suggestions, we know that there are UX gotchas when we start working around the LSP
  - We won't be able to provide as high number of suggestions as in the "Embrace" version because we would increase the response size for every suggestion request (even when the user doesn't want to cycle through more)
  - (Implementation-wise): The engineering effort will be significantly higher

Example user interaction

1. User typed text
2. Language Server fetches suggestion with 4 options and returns a single suggestion ![Screenshot_2024-05-17_at_15.28.51](/uploads/0b8640bb70e2c485cb01ce9af4118d0c/Screenshot_2024-05-17_at_15.28.51.png)
3. The user indicates they want to cycle through (either hover over the suggestion or pressing <kbd>Option</kbd>+<kbd>]</kbd>)
4. Language Server looks into an internal cache, finds the suggestion with 4 options and returns those options (this step is instant) ![Screenshot_2024-05-17_at_15.29.03](/uploads/1c3b8eb763614bcbf13be01f8eb65322/Screenshot_2024-05-17_at_15.29.03.png)

### Recommendation

Based on the analysis, embracing the LSP and making an additional request when the user wants to cycle through suggestions seems to be the preferred approach. It aligns with the LSP's design, requires less engineering effort, and allows for providing more suggestions to the user when needed, albeit with a slight delay.

## Pings

@dashaadu I'll need your feedback on this

@acook.gitlab @jfypk Pinging you since you work on the API, but nothing should change for you (especially since @jfypk already plans to set the candidate limit to some high number (10))

FYI: @timzallmann @kisha.mavryck @mnohr @oregand 

@ohoral, this affects the tracking: Knowing if the user cycled through the suggestions will be trivial. I'm not sure we'll find out how many suggestions they saw.
