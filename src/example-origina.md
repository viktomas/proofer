## Status update 2024-05-17

**TL;DR: we should probably make an additional request when the user wants to cycle through suggestions**

Implementing the UI is a bit more complex than I thought.

- My original thought - We'll be sending [4 suggestion options](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1325#note_1908264625) every time we now send one
- Reality - That's not how the Language Server Protocol is designed

### LSP intended use

- The LS is supposed to return a single option as the user types
- **Only when the user hovers over the inline suggestions (or when they press <kbd>Option</kbd>+<kbd>]</kbd>)** the VS Code asks LS "Give me more suggestions", and the LS is supposed to answer with many (e.g. 10) suggestions

From the docs:

> - automatic trigger - Completion was triggered automatically while editing. It is sufficient to return a single completion item in this case.
> - invoked trigger - Completion was triggered explicitly by a user gesture. Return multiple completion items to enable cycling through them.

<details><summary>Docs</summary>

```ts
/**
 * Describes how an {@link InlineCompletionItemProvider inline completion
 * provider} was triggered.
 *
 * @since 3.18.0
 */
export namespace InlineCompletionTriggerKind {
	/**
	 * Completion was triggered explicitly by a user gesture.
	 * Return multiple completion items to enable cycling through them.
	 */
	export const Invoked: 1 = 1;

	/**
	 * Completion was triggered automatically while editing.
	 * It is sufficient to return a single completion item in this case.
	 */
	export const Automatic: 2 = 2;
}
```

[source](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.18/specification/#textDocument_inlineCompletion)

</details>

### What does that mean for us

We can either **embrace** or **fight** the LSP.

#### Embrace

That means accepting that we'll make an additional request when the user expresses they want multiple suggestions.

- Benefits
  - little to no extra cost because we'll only generate multiple suggestion options when the user wants them
  - we can generate more suggestions for the user (instead of the planned 4)
  - (implementation-wise): easy to track that users made the extra request
- Drawback
  - the user has to wait extra time before getting those options

Example user interaction

1. user typed text
2. LS fetches and returns a single suggestion ![Screenshot_2024-05-17_at_15.28.51](/uploads/0b8640bb70e2c485cb01ce9af4118d0c/Screenshot_2024-05-17_at_15.28.51.png)
3. The user indicates they want to cycle through (either hover over the suggestion or pressing <kbd>Option</kbd>+<kbd>]</kbd>)
4. LS fetches and returns a suggestion with many 10+ options (this step takes extra time because of an API call) ![Screenshot_2024-05-17_at_15.29.03](/uploads/1c3b8eb763614bcbf13be01f8eb65322/Screenshot_2024-05-17_at_15.29.03.png)

#### Fight

We'll try to work around the LSP to provide multiple suggestion options instantly. This would involve some combination of caching and tracing the user's cursor.

- Benefits
  - the moment user wants to cycle through the options, they'll be instantly ready

- Drawbacks
  - form streaming suggestions, we know that there are UX gotchas when we start working around the LSP
  - we won't be able to provide as high number of suggestions as in the "Embrace" version because we would increase the response size for every suggestion request (even when the user doesn't want to cycle through more)
  - (implementation-wise): the engineering effort will be significantly higher

Example user interaction

1. user typed text
2. LS fetches suggestion with 4 options and returns a single suggestion ![Screenshot_2024-05-17_at_15.28.51](/uploads/0b8640bb70e2c485cb01ce9af4118d0c/Screenshot_2024-05-17_at_15.28.51.png)
3. The user indicates they want to cycle through (either hover over the suggestion or pressing <kbd>Option</kbd>+<kbd>]</kbd>)
4. LS looks into an internal cache, finds the suggestion with 4 options and returns those options (this step is instant) ![Screenshot_2024-05-17_at_15.29.03](/uploads/1c3b8eb763614bcbf13be01f8eb65322/Screenshot_2024-05-17_at_15.29.03.png)

## Pings

@dashaadu I'll need your feedback on this

@acook.gitlab @jfypk Pinging you since you work on the API, but nothing should change for you (especially since @jfypk already plans to set the candidate limit to some high number (10))

FYI: @timzallmann @kisha.mavryck @mnohr @oregand 

@ohoral, this affects the tracking: Knowing if the user cycled through the suggestions will be trivial. I'm not sure we'll find out how many suggestions they saw.
