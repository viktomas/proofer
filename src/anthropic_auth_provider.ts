import Anthropic from '@anthropic-ai/sdk';
import vscode from 'vscode';

const tokenToSession = (token: string): vscode.AuthenticationSession => ({
  id: '1',
  account: {
    id: '1',
    label: 'Anthropic token',
  },
  scopes: [],
  accessToken: token,
});

export class AnthropicAuthenticationProvider
  implements vscode.AuthenticationProvider
{
  #secretStorage: vscode.SecretStorage;

  #eventEmitter =
    new vscode.EventEmitter<vscode.AuthenticationProviderAuthenticationSessionsChangeEvent>();

  constructor(secretStorage: vscode.SecretStorage) {
    this.#secretStorage = secretStorage;
  }

  onDidChangeSessions = this.#eventEmitter.event;

  async getSessions(
    scopes?: readonly string[] | undefined
  ): Promise<readonly vscode.AuthenticationSession[]> {
    const token = await this.#secretStorage.get('anthropicToken');
    if (!token) {
      return [];
    }
    return [tokenToSession(token)];
  }

  async createSession(
    scopes: readonly string[]
  ): Promise<vscode.AuthenticationSession> {
    const token = await vscode.window.showInputBox({
      password: true,
      title: 'Insert your anthropic API key',
      ignoreFocusOut: true,
      validateInput: async (token: string) => {
        const anthropic = new Anthropic({ apiKey: token });
        try {
          const message = await anthropic.messages.create({
            max_tokens: 1,
            messages: [{ role: 'user', content: 'Hello' }],
            model: 'claude-3-haiku-20240307',
          });
        } catch (e: any) {
          return `Token validation failed ${e.message}`;
        }
      },
    });
    if (!token) {
      throw new Error('Authentication to Anthropic failed');
    }
    await this.#secretStorage.store('anthropicToken', token);
    const session = tokenToSession(token);
    this.#eventEmitter.fire({ added: [session], removed: [], changed: [] });
    return session;
  }

  async removeSession(sessionId: string): Promise<void> {
    const token = await this.#secretStorage.get('anthropicToken');
    if (!token) {
      return;
    }
    const session = tokenToSession(token);
    await this.#secretStorage.delete('anthropicToken');
    this.#eventEmitter.fire({ added: [], removed: [session], changed: [] });
  }
}
